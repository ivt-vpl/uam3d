package ch.ethz.matsim.uam3d.scene.renderers;

import org.apache.commons.math3.geometry.euclidean.threed.Vector3D;
import org.apache.commons.math3.geometry.euclidean.twod.Vector2D;

import com.jogamp.opengl.GL2;

import ch.ethz.matsim.uam3d.scene.elements.Station;

public class StationRenderer {
	private final Vector3D color = new Vector3D(1.0, 1.0, 0.0);
	private final double maxColorAlpha = 0.7;
	private final double minColorAlpha = 0.3;

	double alphaMultiplier = 0.0;

	public void apply(double dt) {
		alphaMultiplier += 5.0 * dt;

		while (alphaMultiplier > Math.PI * 2.0) {
			alphaMultiplier -= Math.PI * 2.0;
		}
	}

	public void render(GL2 gl, Station station) {
		double height = station.getHeight();

		Vector2D lowerLeft = new Vector2D(-station.getSizeX() * 0.5, -station.getSizeY() * 0.5);
		Vector2D upperLeft = new Vector2D(-station.getSizeX() * 0.5, station.getSizeY() * 0.5);
		Vector2D upperRight = new Vector2D(station.getSizeX() * 0.5, station.getSizeY() * 0.5);
		Vector2D lowerRight = new Vector2D(station.getSizeX() * 0.5, -station.getSizeY() * 0.5);

		gl.glPushMatrix();
		gl.glTranslated(station.getPosition().getX(), station.getPosition().getY(), station.getPosition().getZ());
		// gl.glRotated(vehicle.getAngle(), 0.0, 0.0, 1.0);

		gl.glBegin(GL2.GL_TRIANGLES);
		gl.glColor4d(color.getX(), color.getY(), color.getZ(),
				minColorAlpha + Math.cos(alphaMultiplier) * (maxColorAlpha - minColorAlpha));

		// Top

		gl.glVertex3d(lowerLeft.getX(), lowerLeft.getY(), height);
		gl.glVertex3d(upperLeft.getX(), upperLeft.getY(), height);
		gl.glVertex3d(lowerRight.getX(), lowerRight.getY(), height);

		gl.glVertex3d(upperLeft.getX(), upperLeft.getY(), height);
		gl.glVertex3d(upperRight.getX(), upperRight.getY(), height);
		gl.glVertex3d(lowerRight.getX(), lowerRight.getY(), height);

		// Back

		gl.glVertex3d(upperLeft.getX(), upperLeft.getY(), 0.0);
		gl.glVertex3d(upperLeft.getX(), upperLeft.getY(), height);
		gl.glVertex3d(upperRight.getX(), upperRight.getY(), 0.0);

		gl.glVertex3d(upperLeft.getX(), upperLeft.getY(), height);
		gl.glVertex3d(upperRight.getX(), upperRight.getY(), height);
		gl.glVertex3d(upperRight.getX(), upperRight.getY(), 0.0);

		// Front

		gl.glVertex3d(lowerLeft.getX(), lowerLeft.getY(), 0.0);
		gl.glVertex3d(lowerLeft.getX(), lowerLeft.getY(), height);
		gl.glVertex3d(lowerRight.getX(), lowerRight.getY(), 0.0);

		gl.glVertex3d(lowerLeft.getX(), lowerLeft.getY(), height);
		gl.glVertex3d(lowerRight.getX(), lowerRight.getY(), height);
		gl.glVertex3d(lowerRight.getX(), lowerRight.getY(), 0.0);

		// Left

		gl.glVertex3d(lowerLeft.getX(), lowerLeft.getY(), 0.0);
		gl.glVertex3d(lowerLeft.getX(), lowerLeft.getY(), height);
		gl.glVertex3d(upperLeft.getX(), upperLeft.getY(), 0.0);

		gl.glVertex3d(lowerLeft.getX(), lowerLeft.getY(), height);
		gl.glVertex3d(upperLeft.getX(), upperLeft.getY(), height);
		gl.glVertex3d(upperLeft.getX(), upperLeft.getY(), 0.0);

		// Right

		gl.glVertex3d(lowerRight.getX(), lowerRight.getY(), 0.0);
		gl.glVertex3d(lowerRight.getX(), lowerRight.getY(), height);
		gl.glVertex3d(upperRight.getX(), upperRight.getY(), 0.0);

		gl.glVertex3d(lowerRight.getX(), lowerRight.getY(), height);
		gl.glVertex3d(upperRight.getX(), upperRight.getY(), height);
		gl.glVertex3d(upperRight.getX(), upperRight.getY(), 0.0);

		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glColor3d(color.getX(), color.getY(), color.getZ());

		gl.glVertex3d(lowerLeft.getX(), lowerLeft.getY(), 0.0);
		gl.glVertex3d(upperLeft.getX(), upperLeft.getY(), 0.0);

		gl.glVertex3d(upperLeft.getX(), upperLeft.getY(), 0.0);
		gl.glVertex3d(upperRight.getX(), upperRight.getY(), 0.0);

		gl.glVertex3d(upperRight.getX(), upperRight.getY(), 0.0);
		gl.glVertex3d(lowerRight.getX(), lowerRight.getY(), 0.0);

		gl.glVertex3d(lowerRight.getX(), lowerRight.getY(), 0.0);
		gl.glVertex3d(lowerLeft.getX(), lowerLeft.getY(), 0.0);

		gl.glVertex3d(lowerLeft.getX(), lowerLeft.getY(), height);
		gl.glVertex3d(upperLeft.getX(), upperLeft.getY(), height);

		gl.glVertex3d(upperLeft.getX(), upperLeft.getY(), height);
		gl.glVertex3d(upperRight.getX(), upperRight.getY(), height);

		gl.glVertex3d(upperRight.getX(), upperRight.getY(), height);
		gl.glVertex3d(lowerRight.getX(), lowerRight.getY(), height);

		gl.glVertex3d(lowerRight.getX(), lowerRight.getY(), height);
		gl.glVertex3d(lowerLeft.getX(), lowerLeft.getY(), height);

		gl.glVertex3d(lowerLeft.getX(), lowerLeft.getY(), 0.0);
		gl.glVertex3d(lowerLeft.getX(), lowerLeft.getY(), height);

		gl.glVertex3d(upperLeft.getX(), upperLeft.getY(), 0.0);
		gl.glVertex3d(upperLeft.getX(), upperLeft.getY(), height);

		gl.glVertex3d(upperRight.getX(), upperRight.getY(), 0.0);
		gl.glVertex3d(upperRight.getX(), upperRight.getY(), height);

		gl.glVertex3d(lowerRight.getX(), lowerRight.getY(), 0.0);
		gl.glVertex3d(lowerRight.getX(), lowerRight.getY(), height);

		gl.glEnd();

		gl.glPopMatrix();
	}
}
