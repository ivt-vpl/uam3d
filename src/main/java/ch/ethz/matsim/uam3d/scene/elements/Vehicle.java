package ch.ethz.matsim.uam3d.scene.elements;

import org.apache.commons.math3.geometry.euclidean.threed.Vector3D;
import org.apache.commons.math3.geometry.euclidean.twod.Vector2D;

import ch.ethz.matsim.uam3d.scene.SceneElement;

public class Vehicle implements SceneElement {
	private double length;
	private double width;
	private double height;

	private Vector3D position;
	private Vector2D direction;
	private Vector3D color = new Vector3D(1.0, 0.0, 0.0);

	public Vehicle(double length, double width, double height, Vector3D position, Vector2D direction) {
		this.length = length;
		this.width = width;
		this.height = height;
		this.position = position;
		this.direction = direction;
	}

	public void setPosition(Vector3D position) {
		this.position = position;
	}

	public void setDirection(Vector2D direction) {
		this.direction = direction;
	}

	public double getHeight() {
		return height;
	}

	public double getLength() {
		return length;
	}

	public double getWidth() {
		return width;
	}

	public Vector3D getPosition() {
		return position;
	}

	public Vector2D getDirection() {
		return direction;
	}

	public void setColor(Vector3D color) {
		this.color = color;
	}

	public Vector3D getColor() {
		return color;
	}
}
