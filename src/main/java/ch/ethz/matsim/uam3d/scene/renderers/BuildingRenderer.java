package ch.ethz.matsim.uam3d.scene.renderers;

import org.apache.commons.math3.geometry.euclidean.twod.Vector2D;

import com.jogamp.opengl.GL2;

import ch.ethz.matsim.uam3d.scene.elements.Building;

public class BuildingRenderer {
	public void render(GL2 gl, Building building) {
		double height = building.getHeight();

		Vector2D lowerLeft = new Vector2D(-building.getSizeX() * 0.5, -building.getSizeY() * 0.5);
		Vector2D upperLeft = new Vector2D(-building.getSizeX() * 0.5, building.getSizeY() * 0.5);
		Vector2D upperRight = new Vector2D(building.getSizeX() * 0.5, building.getSizeY() * 0.5);
		Vector2D lowerRight = new Vector2D(building.getSizeX() * 0.5, -building.getSizeY() * 0.5);

		gl.glPushMatrix();
		gl.glTranslated(building.getPosition().getX(), building.getPosition().getY(), 0.0);
		// gl.glRotated(vehicle.getAngle(), 0.0, 0.0, 1.0);

		gl.glBegin(GL2.GL_TRIANGLES);
		gl.glColor4d(1.0, 1.0, 1.0, 0.3);

		// Top

		gl.glVertex3d(lowerLeft.getX(), lowerLeft.getY(), height);
		gl.glVertex3d(upperLeft.getX(), upperLeft.getY(), height);
		gl.glVertex3d(lowerRight.getX(), lowerRight.getY(), height);

		gl.glVertex3d(upperLeft.getX(), upperLeft.getY(), height);
		gl.glVertex3d(upperRight.getX(), upperRight.getY(), height);
		gl.glVertex3d(lowerRight.getX(), lowerRight.getY(), height);

		// Back

		gl.glVertex3d(upperLeft.getX(), upperLeft.getY(), 0.0);
		gl.glVertex3d(upperLeft.getX(), upperLeft.getY(), height);
		gl.glVertex3d(upperRight.getX(), upperRight.getY(), 0.0);

		gl.glVertex3d(upperLeft.getX(), upperLeft.getY(), height);
		gl.glVertex3d(upperRight.getX(), upperRight.getY(), height);
		gl.glVertex3d(upperRight.getX(), upperRight.getY(), 0.0);

		// Front

		gl.glVertex3d(lowerLeft.getX(), lowerLeft.getY(), 0.0);
		gl.glVertex3d(lowerLeft.getX(), lowerLeft.getY(), height);
		gl.glVertex3d(lowerRight.getX(), lowerRight.getY(), 0.0);

		gl.glVertex3d(lowerLeft.getX(), lowerLeft.getY(), height);
		gl.glVertex3d(lowerRight.getX(), lowerRight.getY(), height);
		gl.glVertex3d(lowerRight.getX(), lowerRight.getY(), 0.0);

		// Left

		gl.glVertex3d(lowerLeft.getX(), lowerLeft.getY(), 0.0);
		gl.glVertex3d(lowerLeft.getX(), lowerLeft.getY(), height);
		gl.glVertex3d(upperLeft.getX(), upperLeft.getY(), 0.0);

		gl.glVertex3d(lowerLeft.getX(), lowerLeft.getY(), height);
		gl.glVertex3d(upperLeft.getX(), upperLeft.getY(), height);
		gl.glVertex3d(upperLeft.getX(), upperLeft.getY(), 0.0);

		// Right

		gl.glVertex3d(lowerRight.getX(), lowerRight.getY(), 0.0);
		gl.glVertex3d(lowerRight.getX(), lowerRight.getY(), height);
		gl.glVertex3d(upperRight.getX(), upperRight.getY(), 0.0);

		gl.glVertex3d(lowerRight.getX(), lowerRight.getY(), height);
		gl.glVertex3d(upperRight.getX(), upperRight.getY(), height);
		gl.glVertex3d(upperRight.getX(), upperRight.getY(), 0.0);

		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glColor3d(1.0, 1.0, 1.0);

		gl.glVertex3d(lowerLeft.getX(), lowerLeft.getY(), 0.0);
		gl.glVertex3d(upperLeft.getX(), upperLeft.getY(), 0.0);

		gl.glVertex3d(upperLeft.getX(), upperLeft.getY(), 0.0);
		gl.glVertex3d(upperRight.getX(), upperRight.getY(), 0.0);

		gl.glVertex3d(upperRight.getX(), upperRight.getY(), 0.0);
		gl.glVertex3d(lowerRight.getX(), lowerRight.getY(), 0.0);

		gl.glVertex3d(lowerRight.getX(), lowerRight.getY(), 0.0);
		gl.glVertex3d(lowerLeft.getX(), lowerLeft.getY(), 0.0);

		gl.glVertex3d(lowerLeft.getX(), lowerLeft.getY(), height);
		gl.glVertex3d(upperLeft.getX(), upperLeft.getY(), height);

		gl.glVertex3d(upperLeft.getX(), upperLeft.getY(), height);
		gl.glVertex3d(upperRight.getX(), upperRight.getY(), height);

		gl.glVertex3d(upperRight.getX(), upperRight.getY(), height);
		gl.glVertex3d(lowerRight.getX(), lowerRight.getY(), height);

		gl.glVertex3d(lowerRight.getX(), lowerRight.getY(), height);
		gl.glVertex3d(lowerLeft.getX(), lowerLeft.getY(), height);

		gl.glVertex3d(lowerLeft.getX(), lowerLeft.getY(), 0.0);
		gl.glVertex3d(lowerLeft.getX(), lowerLeft.getY(), height);

		gl.glVertex3d(upperLeft.getX(), upperLeft.getY(), 0.0);
		gl.glVertex3d(upperLeft.getX(), upperLeft.getY(), height);

		gl.glVertex3d(upperRight.getX(), upperRight.getY(), 0.0);
		gl.glVertex3d(upperRight.getX(), upperRight.getY(), height);

		gl.glVertex3d(lowerRight.getX(), lowerRight.getY(), 0.0);
		gl.glVertex3d(lowerRight.getX(), lowerRight.getY(), height);

		gl.glEnd();

		gl.glPopMatrix();
	}
}
