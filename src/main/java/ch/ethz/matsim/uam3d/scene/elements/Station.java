package ch.ethz.matsim.uam3d.scene.elements;

import org.apache.commons.math3.geometry.euclidean.threed.Vector3D;

import ch.ethz.matsim.uam3d.scene.SceneElement;

public class Station implements SceneElement {
	private double sizeX;
	private double sizeY;
	private double height;
	private Vector3D position;

	public Station(double sizeX, double sizeY, double height, Vector3D position) {
		this.sizeX = sizeX;
		this.sizeY = sizeY;
		this.height = height;
		this.position = position;
	}

	public double getHeight() {
		return height;
	}

	public double getSizeX() {
		return sizeX;
	}

	public double getSizeY() {
		return sizeY;
	}

	public Vector3D getPosition() {
		return position;
	}
}