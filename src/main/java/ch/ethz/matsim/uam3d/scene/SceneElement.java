package ch.ethz.matsim.uam3d.scene;

import org.apache.commons.math3.geometry.euclidean.threed.Vector3D;

public interface SceneElement {
	Vector3D getPosition();
}
