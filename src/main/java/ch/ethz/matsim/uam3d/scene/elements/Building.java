package ch.ethz.matsim.uam3d.scene.elements;

import org.apache.commons.math3.geometry.euclidean.threed.Vector3D;
import org.apache.commons.math3.geometry.euclidean.twod.Vector2D;

import ch.ethz.matsim.uam3d.scene.SceneElement;

public class Building implements SceneElement {
	private double sizeX;
	private double sizeY;
	private double height;
	private Vector3D position;

	public Building(double sizeX, double sizeY, double height, Vector2D position) {
		this.sizeX = sizeX;
		this.sizeY = sizeY;
		this.height = height;
		this.position = new Vector3D(position.getX(), position.getY(), 0.0);
	}

	public double getHeight() {
		return height;
	}

	public double getSizeX() {
		return sizeX;
	}

	public double getSizeY() {
		return sizeY;
	}

	public Vector3D getPosition() {
		return position;
	}
}
