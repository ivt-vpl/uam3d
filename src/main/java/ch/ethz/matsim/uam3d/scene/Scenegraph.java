package ch.ethz.matsim.uam3d.scene;

import java.util.Comparator;
import java.util.Iterator;
import java.util.PriorityQueue;

import org.apache.commons.math3.geometry.euclidean.threed.Vector3D;

public class Scenegraph implements Iterable<SceneElement> {
	private final Vector3D cameraPosition;

	private PriorityQueue<SceneElement> elements = new PriorityQueue<>(new Comparator<SceneElement>() {
		@Override
		public int compare(SceneElement o1, SceneElement o2) {
			double d1 = cameraPosition.distanceSq(o1.getPosition());
			double d2 = cameraPosition.distanceSq(o2.getPosition());
			return Double.compare(d1, d2);
		}
	});

	public Scenegraph(Vector3D cameraPosition) {
		this.cameraPosition = cameraPosition;
	}

	public void addElement(SceneElement element) {
		elements.add(element);
	}

	@Override
	public Iterator<SceneElement> iterator() {
		return elements.iterator();
	}
}
