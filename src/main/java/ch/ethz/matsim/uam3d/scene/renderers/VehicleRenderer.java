package ch.ethz.matsim.uam3d.scene.renderers;

import org.apache.commons.math3.geometry.euclidean.threed.Vector3D;
import org.apache.commons.math3.geometry.euclidean.twod.Vector2D;

import com.jogamp.opengl.GL2;

import ch.ethz.matsim.uam3d.scene.elements.Vehicle;

public class VehicleRenderer {
	public void render(GL2 gl, Vehicle vehicle) {
		double height = vehicle.getHeight();

		Vector2D lowerLeft = new Vector2D(-vehicle.getLength() * 0.5, -vehicle.getWidth() * 0.5);
		Vector2D upperLeft = new Vector2D(-vehicle.getLength() * 0.5, vehicle.getWidth() * 0.5);
		Vector2D upperRight = new Vector2D(vehicle.getLength() * 0.5, vehicle.getWidth() * 0.5);
		Vector2D lowerRight = new Vector2D(vehicle.getLength() * 0.5, -vehicle.getWidth() * 0.5);

		gl.glPushMatrix();
		gl.glTranslated(vehicle.getPosition().getX(), vehicle.getPosition().getY(), vehicle.getPosition().getZ());

		double angle = Math.atan2(vehicle.getDirection().getY(), vehicle.getDirection().getX());
		gl.glRotated(angle * 360.0 / (2.0 * Math.PI), 0.0, 0.0, 1.0);

		Vector3D color = vehicle.getColor();
		gl.glColor4d(color.getX(), color.getY(), color.getZ(), 0.7);

		gl.glBegin(GL2.GL_TRIANGLES);

		// Top

		gl.glVertex3d(lowerLeft.getX(), lowerLeft.getY(), height);
		gl.glVertex3d(upperLeft.getX(), upperLeft.getY(), height);
		gl.glVertex3d(lowerRight.getX(), lowerRight.getY(), height);

		gl.glVertex3d(upperLeft.getX(), upperLeft.getY(), height);
		gl.glVertex3d(upperRight.getX(), upperRight.getY(), height);
		gl.glVertex3d(lowerRight.getX(), lowerRight.getY(), height);

		// Back

		gl.glVertex3d(upperLeft.getX(), upperLeft.getY(), 0.0);
		gl.glVertex3d(upperLeft.getX(), upperLeft.getY(), height);
		gl.glVertex3d(upperRight.getX(), upperRight.getY(), 0.0);

		gl.glVertex3d(upperLeft.getX(), upperLeft.getY(), height);
		gl.glVertex3d(upperRight.getX(), upperRight.getY(), height);
		gl.glVertex3d(upperRight.getX(), upperRight.getY(), 0.0);

		// Front

		gl.glVertex3d(lowerLeft.getX(), lowerLeft.getY(), 0.0);
		gl.glVertex3d(lowerLeft.getX(), lowerLeft.getY(), height);
		gl.glVertex3d(lowerRight.getX(), lowerRight.getY(), 0.0);

		gl.glVertex3d(lowerLeft.getX(), lowerLeft.getY(), height);
		gl.glVertex3d(lowerRight.getX(), lowerRight.getY(), height);
		gl.glVertex3d(lowerRight.getX(), lowerRight.getY(), 0.0);

		// Left

		gl.glVertex3d(lowerLeft.getX(), lowerLeft.getY(), 0.0);
		gl.glVertex3d(lowerLeft.getX(), lowerLeft.getY(), height);
		gl.glVertex3d(upperLeft.getX(), upperLeft.getY(), 0.0);

		gl.glVertex3d(lowerLeft.getX(), lowerLeft.getY(), height);
		gl.glVertex3d(upperLeft.getX(), upperLeft.getY(), height);
		gl.glVertex3d(upperLeft.getX(), upperLeft.getY(), 0.0);

		// Right

		gl.glVertex3d(lowerRight.getX(), lowerRight.getY(), 0.0);
		gl.glVertex3d(lowerRight.getX(), lowerRight.getY(), height);
		gl.glVertex3d(upperRight.getX(), upperRight.getY(), 0.0);

		gl.glVertex3d(lowerRight.getX(), lowerRight.getY(), height);
		gl.glVertex3d(upperRight.getX(), upperRight.getY(), height);
		gl.glVertex3d(upperRight.getX(), upperRight.getY(), 0.0);

		gl.glEnd();

		gl.glBegin(GL2.GL_LINES);
		gl.glColor3d(color.getX(), color.getY(), color.getZ());

		gl.glVertex3d(lowerLeft.getX(), lowerLeft.getY(), 0.0);
		gl.glVertex3d(upperLeft.getX(), upperLeft.getY(), 0.0);

		gl.glVertex3d(upperLeft.getX(), upperLeft.getY(), 0.0);
		gl.glVertex3d(upperRight.getX(), upperRight.getY(), 0.0);

		gl.glVertex3d(upperRight.getX(), upperRight.getY(), 0.0);
		gl.glVertex3d(lowerRight.getX(), lowerRight.getY(), 0.0);

		gl.glVertex3d(lowerRight.getX(), lowerRight.getY(), 0.0);
		gl.glVertex3d(lowerLeft.getX(), lowerLeft.getY(), 0.0);

		gl.glVertex3d(lowerLeft.getX(), lowerLeft.getY(), height);
		gl.glVertex3d(upperLeft.getX(), upperLeft.getY(), height);

		gl.glVertex3d(upperLeft.getX(), upperLeft.getY(), height);
		gl.glVertex3d(upperRight.getX(), upperRight.getY(), height);

		gl.glVertex3d(upperRight.getX(), upperRight.getY(), height);
		gl.glVertex3d(lowerRight.getX(), lowerRight.getY(), height);

		gl.glVertex3d(lowerRight.getX(), lowerRight.getY(), height);
		gl.glVertex3d(lowerLeft.getX(), lowerLeft.getY(), height);

		gl.glVertex3d(lowerLeft.getX(), lowerLeft.getY(), 0.0);
		gl.glVertex3d(lowerLeft.getX(), lowerLeft.getY(), height);

		gl.glVertex3d(upperLeft.getX(), upperLeft.getY(), 0.0);
		gl.glVertex3d(upperLeft.getX(), upperLeft.getY(), height);

		gl.glVertex3d(upperRight.getX(), upperRight.getY(), 0.0);
		gl.glVertex3d(upperRight.getX(), upperRight.getY(), height);

		gl.glVertex3d(lowerRight.getX(), lowerRight.getY(), 0.0);
		gl.glVertex3d(lowerRight.getX(), lowerRight.getY(), height);

		gl.glEnd();

		gl.glPopMatrix();
	}
}
