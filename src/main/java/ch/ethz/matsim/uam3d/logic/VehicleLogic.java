package ch.ethz.matsim.uam3d.logic;

import java.util.Random;

import org.apache.commons.math3.geometry.euclidean.threed.Vector3D;
import org.apache.commons.math3.geometry.euclidean.twod.Vector2D;

import ch.ethz.matsim.uam3d.scene.elements.Vehicle;

public class VehicleLogic {
	private final Random random;
	private final Vehicle vehicle;

	private Vector3D vehicleEndPosition = new Vector3D(1.0, 1.0, 0.0);
	private Vector3D vehicleStartPosition = new Vector3D(1.0, 0.0, 0.0);
	private Vector3D vehiclePosition = new Vector3D(1.0, 1.0, 0.0);
	private Vector2D vehicleDirection = new Vector2D(1.0, 0.0).normalize();
	private double s = 1.0;
	private boolean previousWasZ = true;

	public VehicleLogic(Vehicle vehicle, Random random) {
		this.vehicle = vehicle;
		this.random = random;

		this.vehicleStartPosition = vehicle.getPosition();
		this.vehicleEndPosition = vehicle.getPosition();
		this.vehiclePosition = vehicle.getPosition();
		this.vehicleDirection = vehicle.getDirection();
	}

	public void simulate(double dt) {
		s += 2.0 * dt;

		if (s >= 1.0) {
			Vector2D sideDirection = new Vector2D(-vehicleDirection.getY(), vehicleDirection.getX()).normalize();

			vehicleStartPosition = new Vector3D(vehicleEndPosition.getX(), vehicleEndPosition.getY(),
					vehicleEndPosition.getZ());

			Vector3D newPosition;
			Vector2D newDirection;
			boolean newPreviousWasZ = false;

			while (true) {
				newPosition = vehicleStartPosition.scalarMultiply(1.0);
				newDirection = vehicleDirection.scalarMultiply(1.0);

				if (random.nextDouble() < 0.2 && !previousWasZ) {
					// Go up or down

					if (random.nextDouble() < 0.5) {
						newPosition = new Vector3D(newPosition.getX(), newPosition.getY(), newPosition.getZ() + 1.0);
					} else {
						newPosition = new Vector3D(newPosition.getX(), newPosition.getY(), newPosition.getZ() - 1.0);
					}

					newPreviousWasZ = true;
				} else {
					// Go straight or left or right

					if (random.nextDouble() < 0.8) {
						newPosition = newPosition.add(2.0,
								new Vector3D(vehicleDirection.getX(), vehicleDirection.getY(), 0.0));
					} else {
						double f = random.nextDouble() < 0.5 ? -1.0 : 1.0;
						newDirection = sideDirection.scalarMultiply(f).normalize();
						newPosition = newPosition.add(2.0, new Vector3D(newDirection.getX(), newDirection.getY(), 0.0));
					}
				}

				if (newPosition.getZ() < 0.0 || newPosition.getZ() > 3.0) {
					continue;
				}

				if (newPosition.getX() < -2.0 || newPosition.getX() > 20) {
					continue;
				}

				if (newPosition.getY() < -2.0 || newPosition.getY() > 20) {
					continue;
				}

				break;
			}

			s = 0.0;
			previousWasZ = newPreviousWasZ;
			vehicleEndPosition = newPosition;
			vehicleDirection = newDirection;
		}

		vehiclePosition = vehicleStartPosition.add(vehicleEndPosition.subtract(vehicleStartPosition).scalarMultiply(s));

		vehicle.setPosition(vehiclePosition);
		vehicle.setDirection(vehicleDirection);
	}
}
