package ch.ethz.matsim.uam3d.rendering;

import org.apache.commons.math3.geometry.euclidean.threed.Vector3D;

import com.jogamp.opengl.GL2;

public class RotationCamera {
	private final Camera camera;

	private double currentAngle = 0.0;

	private final double radius = 25.0;
	private final double height = 15.0;
	private final double speed = 0.2;
	private final Vector3D target = new Vector3D(10.0, 10.0, 0.0);

	public RotationCamera(Camera camera) {
		this.camera = camera;
	}

	public void apply(GL2 gl, double dt) {
		currentAngle += speed * dt;

		Vector3D position = new Vector3D(Math.cos(currentAngle), Math.sin(currentAngle), 0.0).scalarMultiply(radius)
				.add(new Vector3D(0.0, 0.0, height)).add(target);

		camera.setPosition(position);
		camera.setTarget(target);

		camera.apply(gl);
	}
}
