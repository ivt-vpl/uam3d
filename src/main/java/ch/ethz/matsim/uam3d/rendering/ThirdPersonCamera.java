package ch.ethz.matsim.uam3d.rendering;

import org.apache.commons.math3.geometry.euclidean.threed.Vector3D;

import com.jogamp.opengl.GL2;

import ch.ethz.matsim.uam3d.scene.elements.Vehicle;

public class ThirdPersonCamera {
	private final Camera camera;

	private double currentAngle = Double.NaN;

	private final double distance = 4.0;
	private final double height = 1.0;

	public ThirdPersonCamera(Camera camera) {
		this.camera = camera;
	}

	public void apply(Vehicle vehicle, GL2 gl, double dt) {
		double referenceAngle = Math.atan2(vehicle.getDirection().getY(), vehicle.getDirection().getX());

		if (Double.isNaN(currentAngle)) {
			currentAngle = referenceAngle;
		} else {
			currentAngle += dt * 5.0 * (referenceAngle - currentAngle);
		}

		while (currentAngle > 2.0 * Math.PI) {
			currentAngle -= 2.0 * Math.PI;
		}

		Vector3D direction = new Vector3D(Math.cos(currentAngle), Math.sin(currentAngle), 0.0);
		Vector3D position = vehicle.getPosition().subtract(distance, direction).add(new Vector3D(0.0, 0.0, height));

		camera.setPosition(position);
		camera.setTarget(vehicle.getPosition());

		camera.apply(gl);
	}
}
