package ch.ethz.matsim.uam3d;

import java.util.LinkedList;
import java.util.List;
import java.util.Random;

import javax.swing.JFrame;

import org.apache.commons.math3.geometry.euclidean.threed.Vector3D;
import org.apache.commons.math3.geometry.euclidean.twod.Vector2D;

import com.jogamp.opengl.GL2;
import com.jogamp.opengl.GLAutoDrawable;
import com.jogamp.opengl.GLCapabilities;
import com.jogamp.opengl.GLEventListener;
import com.jogamp.opengl.GLProfile;
import com.jogamp.opengl.awt.GLCanvas;
import com.jogamp.opengl.util.FPSAnimator;

import ch.ethz.matsim.uam3d.logic.VehicleLogic;
import ch.ethz.matsim.uam3d.rendering.Camera;
import ch.ethz.matsim.uam3d.rendering.Perspective;
import ch.ethz.matsim.uam3d.rendering.RotationCamera;
import ch.ethz.matsim.uam3d.rendering.ThirdPersonCamera;
import ch.ethz.matsim.uam3d.scene.SceneElement;
import ch.ethz.matsim.uam3d.scene.Scenegraph;
import ch.ethz.matsim.uam3d.scene.elements.Building;
import ch.ethz.matsim.uam3d.scene.elements.Station;
import ch.ethz.matsim.uam3d.scene.elements.Vehicle;
import ch.ethz.matsim.uam3d.scene.renderers.BuildingRenderer;
import ch.ethz.matsim.uam3d.scene.renderers.StationRenderer;
import ch.ethz.matsim.uam3d.scene.renderers.VehicleRenderer;

public class RunVisualization extends JFrame implements GLEventListener {
	static public void main(String[] args) {
		new RunVisualization();
	}

	private GLCanvas canvas;
	private Perspective perspective;
	private Camera camera;
	private ThirdPersonCamera thirdPersonCamera;
	private RotationCamera rotationCamera;

	public RunVisualization() {
		super("UAM");

		this.setSize(800, 800);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		GLProfile profile = GLProfile.get(GLProfile.GL2);
		GLCapabilities capabilities = new GLCapabilities(profile);
		capabilities.setSampleBuffers(true);

		perspective = new Perspective(800.0, 800.0, 45.0, 0.01, 100.0);

		camera = new Camera(perspective);

		camera.setPosition(new Vector3D(-5.0, -7.0, 10.0));
		camera.setTarget(new Vector3D(10.0, 10.0, 0.0));
		camera.setUp(new Vector3D(0.0, 0.0, 1.0));

		thirdPersonCamera = new ThirdPersonCamera(camera);
		rotationCamera = new RotationCamera(camera);

		Random random = new Random(0);

		for (int i = 0; i < 10; i++) {
			for (int j = 0; j < 10; j++) {
				if (random.nextDouble() < 0.9) {
					double x = i * 2.0;
					double y = j * 2.0;
					double height = random.nextInt(3) + 1.0;

					Building building = new Building(1.0, 1.0, height, new Vector2D(x, y));
					buildings.add(building);

					if (random.nextDouble() < 0.1) {
						Station station = new Station(1.0, 1.0, 0.1, new Vector3D(x, y, height));
						stations.add(station);
					}
				}
			}
		}

		for (int k = 0; k < 20; k++) {
			Vector3D position = new Vector3D(3.0 + random.nextInt(5) * 2.0, 1.0 + random.nextInt(5) * 2.0,
					random.nextInt(3));

			Vector2D direction = random.nextDouble() < 0.5 ? new Vector2D(1.0, 0.0) : new Vector2D(0.0, 1.0);
			direction = direction.scalarMultiply(random.nextDouble() < 0.5 ? 1.0 : -1.0);
			direction = direction.normalize();

			Vector3D color = new Vector3D(random.nextDouble() * 0.7 + 0.3, random.nextDouble() * 0.7 + 0.3,
					random.nextDouble() * 0.7 + 0.3);

			Vehicle vehicle = new Vehicle(0.5, 0.2, 0.2, position, direction);
			VehicleLogic logic = new VehicleLogic(vehicle, random);

			vehicle.setColor(color);

			vehicles.add(vehicle);
			vehicleLogics.add(logic);
		}

		previousTime = System.nanoTime();

		canvas = new GLCanvas(capabilities);
		canvas.addGLEventListener(this);
		this.getContentPane().add(canvas);

		this.setVisible(true);
		this.setResizable(true);
		canvas.requestFocusInWindow();

		FPSAnimator animator = new FPSAnimator(canvas, 60);
		animator.start();
	}

	private long previousTime = 0;

	private final List<Station> stations = new LinkedList<>();
	private final StationRenderer stationRenderer = new StationRenderer();

	private final List<Building> buildings = new LinkedList<>();
	private final BuildingRenderer buildingRenderer = new BuildingRenderer();

	private final List<Vehicle> vehicles = new LinkedList<>();
	private final List<VehicleLogic> vehicleLogics = new LinkedList<>();
	private final VehicleRenderer vehicleRenderer = new VehicleRenderer();

	@Override
	public void init(GLAutoDrawable drawable) {
	}

	@Override
	public void dispose(GLAutoDrawable drawable) {
	}

	@Override
	public void display(GLAutoDrawable drawable) {
		long currentTime = System.nanoTime();
		double dt = (currentTime - previousTime) * 1e-9;
		previousTime = currentTime;

		GL2 gl = drawable.getGL().getGL2();
		gl.glEnable(gl.GL_BLEND);
		gl.glBlendFunc(gl.GL_SRC_ALPHA, gl.GL_ONE_MINUS_SRC_ALPHA);

		gl.glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
		gl.glClear(GL2.GL_COLOR_BUFFER_BIT | GL2.GL_DEPTH_BUFFER_BIT);

		gl.glMatrixMode(GL2.GL_MODELVIEW);
		gl.glLoadIdentity();

		vehicleLogics.forEach(logic -> logic.simulate(dt));
		//thirdPersonCamera.apply(vehicles.get(0), gl, dt);
		rotationCamera.apply(gl, dt);

		stationRenderer.apply(dt);

		Scenegraph graph = new Scenegraph(camera.getPosition());

		for (Building building : buildings) {
			graph.addElement(building);
		}

		for (Vehicle vehicle : vehicles) {
			graph.addElement(vehicle);
		}

		for (Station station : stations) {
			graph.addElement(station);
		}

		for (SceneElement element : graph) {
			if (element instanceof Building) {
				buildingRenderer.render(gl, (Building) element);
			}

			if (element instanceof Vehicle) {
				vehicleRenderer.render(gl, (Vehicle) element);
			}

			if (element instanceof Station) {
				stationRenderer.render(gl, (Station) element);
			}
		}

		/*
		 * for (Building building : buildings) { buildingRenderer.render(gl, building);
		 * }
		 * 
		 * for (Vehicle vehicle : vehicles) { vehicleRenderer.render(gl, vehicle); }
		 */

		// camera.setPosition(new Vector3D(cameraX, cameraY, 10.0));
		// camera.setTarget(vehiclePosition);

		// Vector3D direction = new Vector3D(vehicleDirection.getX(),
		// vehicleDirection.getY(), 0.0);

		// camera.setPosition(vehiclePosition.add(direction.scalarMultiply(-5.0).add(new
		// Vector3D(0.0, 0.0, 5.0))));
		// camera.setTarget(vehiclePosition);

		// camera.apply(gl);

		// Vehicle vehicle = new Vehicle(0.5, 0.2, 0.2, vehiclePosition,
		// vehicleDirection);

		// thirdPersonCamera.apply(vehicle, gl, dt);

		// for (Building building : buildings) {
		// buildingRenderer.render(gl, building);
		// }

		// new VehicleRenderer().render(gl, vehicle);
	}

	@Override
	public void reshape(GLAutoDrawable drawable, int x, int y, int width, int height) {
		GL2 gl = drawable.getGL().getGL2();
		gl.glViewport(0, 0, width, height);

		perspective.update(width, height);
		perspective.apply(gl);
	}
}
